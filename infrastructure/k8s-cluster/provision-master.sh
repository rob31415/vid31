#! /bin/bash

echo "**** PROVISIONING MASTER"

apt install -y kubernetes-cni

# [WARNING Mem]: the system RAM (976 MB) is less than the minimum 1700 MB
kubeadm init --pod-network-cidr 10.244.0.0/16 --apiserver-advertise-address 192.168.56.10 --ignore-preflight-errors=Mem

# i know.. atrocious. replace ="  with  ="blabla  effectively prepending
sed 's/=\"/=\"--node-ip=192\.168\.56\.10 /' /var/lib/kubelet/kubeadm-flags.env > /tmp/bla && mv /tmp/bla /var/lib/kubelet/kubeadm-flags.env
systemctl daemon-reload && systemctl restart kubelet

# this is for the workers which are being created after master
kubeadm token create --print-join-command > /vagrant/join.sh
chmod +x /vagrant/join.sh

# that's mandatory otherwise nodes don't get ready
kubectl apply -f /vagrant/flannel.yml

echo 'source <(kubectl completion bash)' >>~/.bashrc
