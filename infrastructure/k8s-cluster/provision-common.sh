#! /bin/bash

echo "**** Doing some base init stuff now..."

apt update --allow-releaseinfo-change
apt install -y curl gnupg2 htop
#apt upgrade -y

# avoid [ERROR FileContent--proc-sys-net-ipv4-ip_forward]: /proc/sys/net/ipv4/ip_forward contents are not set to 1
echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
sysctl --system

# avoid [ERROR FileContent--proc-sys-net-bridge-bridge-nf-call-iptables]: /proc/sys/net/bridge/bridge-nf-call-iptables does not exist
update-alternatives --set iptables /usr/sbin/iptables-legacy
update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
update-alternatives --set ebtables /usr/sbin/ebtables-legacy
modprobe br_netfilter

echo "**** Instlng k8s"

echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >> /etc/bash.bashrc
echo "alias k='kubectl'" >> /etc/bash.bashrc

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main"  >> /etc/apt/sources.list.d/kubernetes.list
apt-get update --allow-releaseinfo-change

# kubelet 1.26 breaking change (var/run/containerd/containerd.sock doesnt support API v1)
# source:  https://serverfault.com/questions/1118051/failed-to-run-kubelet-validate-service-connection-cri-v1-runtime-api-is-not-im
apt install -y kubelet=1.25.5-00 kubectl kubeadm containerd net-tools 


# use cgroups v1. default is v2 which leads to a mysterious sigterm being sent to etcd - a crash most likely :-/
sed 's/^GRUB_CMDLINE_LINUX_DEFAULT.*//' /etc/default/grub > /etc/default/grub     # remove line
echo 'GRUB_CMDLINE_LINUX_DEFAULT="net.ifnames=0 biosdevname=0 systemd.unified_cgroup_hierarchy=0"' >> /etc/default/grub # put new line
update-grub
# a reboot is neccessary now (manually for the time being... :-/)

# make crictl work
echo "runtime-endpoint: unix:///var/run/containerd/containerd.sock" >> /etc/crictl.yaml
echo "image-endpoint: unix:///var/run/containerd/containerd.sock" >> /etc/crictl.yaml
echo "timeout: 10" >> /etc/crictl.yaml
echo "#debug: true" >> /etc/crictl.yaml

# flannel wants that there
mkdir /opt/bin
cp /vagrant/flanneld-amd64 /opt/bin/flanneld

# coredns wants those there
mkdir /usr/lib/cni
#cp /opt/cni/bin/{ridge,flannel,host-local,loopback,portmap} /usr/lib/cni/  ridge not there
cp /opt/cni/bin/{flannel,host-local,loopback,portmap,bridge} /usr/lib/cni/
