# meta

This is supposed to let you set up a "k8s on VMs installation" very easily.

# dependencies

tested to work on 06.01.2023 w/

- VirtualBox 6.1.40 r154048 
- Vagrant 2.3.4

# notes

after vagrant up, unfortunately you have to:

1. restart master
2. manually install flannel (see provision-master.sh how to do "kubectl apply flannel...")

once etcd can handle cgroups2, this goes away.
