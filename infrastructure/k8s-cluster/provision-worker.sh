#! /bin/bash

echo "**** PROVISIONING WORKER #$1"

/vagrant/join.sh

cp /etc/kubernetes/kubelet.conf /etc/kubernetes/admin.conf

# i know.. atrocious. replace ="  with  ="blabla  effectively prepending; and do calc in var-substitution too.. ugh
sed "s:=\":=\"--node-ip=192\.168\.57\.$((10+$1)) :" /var/lib/kubelet/kubeadm-flags.env > /tmp/bla && mv /tmp/bla /var/lib/kubelet/kubeadm-flags.env
systemctl daemon-reload && systemctl restart kubelet
