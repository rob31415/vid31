# meta

This is an example project for the purpose of learning.

It should be fully functional and easily deployable to prove proficiency regarding the utilized technologies.

# application description

- "vid31" is a web application, which lets users maintain lists of their favourite youtube videos.
- The user gets video suggestions based on what their lists contain and also based on some rss feeds.
- There is a browser plug-in, which lets users comfortably add youtube videos to their lists.
- "vid31" could be pronounced "vid thirtee-one" or "videl" ...
- to deviate from 42 for once, 31 is 4-1 and 2-1

# architecture and technologies used

- distributed system
- microservice architecture
- Java, SpringBoot or JakartaEE & Quarkus, Python, Kafka, Kubernetes, Istio, ReactJS, JWT

## current skills

During learning, this will improve.


                                   know of it
                                   |       know about it (caps/ecosystem/relations)
                                   |       |       know config
                        LEVEL:     |       |       |       can utilize it threshold
                                   |       |       |       |       been there, done that
                                   |       |       |       |       |
                                   |       |       |       |       |
                                   |       |       |       |       |
        TECH:
                
        kafka                      |------------>
        microservice architekture  |------------------>
        spring                     |------------->
        quarkus                    |>
        k8s                        |------------------>
        service-meshes (istio)     |------>      
        reactJS                    |------------------------>
        JWT                        |----->
        WebSock                    |------->
        Python                     |-------------------------------->


# services and VM distribution

- let's start with 4 VMs w/ k8s installed on all of them
- then, this project has the following units (apps/deployments/services/...)

        app              #replicas      namespace

        image-registry   1              devops
        proxy            1              ingress
        web              2              frontend
        DB               2              db
        kafka-broker     3              messaging
        kafka-zookeeper  3              messaging
        users MS         2              backend
        vlists MS        2              backend
        suggs MS         2              backend
        feed MS          2              backend
        API-gateway      2              frontend

## usage of kafka

taken from: Kafka and The Service Mesh | Gwen Shapira, Confluent

> "vision #3 : kafka as a service in a mesh"

Meaning, services talk to service-mesh proxy, which goes to kafka - for both sending and receiving.

# use cases and flows

![Sequence Diagram](use-cases.svg "Sequence Diagram of happy flow main use cases")

## description

- The end-user only ever talks to the proxy.
- The proxy goes to either one of the webservers or to one of the API gateways.
        - login provides an JWT to authenticate all other requests
- Behind the API gateways there's async messaging happening amongst the different services.
        - API gateway is a translation between sync and async communication
- The suggestions are not request/reply, but they are reactive, meaning, they're pushed to the user via websocket.
- suggestionService and feedService each work with their own, technically independent frequency

## source of the diagram

        @startuml
        skinparam backgroundColor #EEEBEF
        skinparam handwritten false
        skinparam BoxPadding 10

        skinparam sequence {
        componentArrowFontColor Sienna
        ArrowColor LightSlateGrey
        ParticipantBorderColor SaddleBrown
        ParticipantBackgroundColor Olive
        ParticipantFontSize 20
        ParticipantFontColor White
        CollectionsBorderColor Blue
        BackgroundColor Olive
        }

        skinparam collections {
        FontColor White
        FontSize 20
        BackGroundColor Olive
        }

        actor user

        participant proxy

        collections webserver

        collections APIGateway

        box "user microservice"
        queue       userReq
        queue       userRep
        collections userService
        entity      jwt
        database    userDb
        endbox

        box "videolist microservice"
        queue       vlistReq
        queue       vlistRep
        collections vlistService
        database    vlistDb
        endbox

        box "suggestion microservice"
        queue       suggestReq
        queue       suggestRep
        collections suggestionService
        end box

        box "feed collector microservice"
        queue       feedReq
        queue       feedRep
        collections feedService
        end box

        collections  externalWebsites

        == init ==

        user -> proxy
        proxy -> webserver
        webserver -> proxy  : deliver SPA
        proxy -> user

        == login ==

        user -> proxy : login w/ uname/pw
        proxy -> APIGateway : login
        APIGateway --> userReq : login
        userReq --> userService : login
        userService -> userDb : read
        userService -> jwt : create
        userRep <-- userService : JWT
        APIGateway <-- userRep : JWT
        proxy <- APIGateway : JWT
        user <- proxy : JWT

        == get videolist ==

        user -> proxy : get videolist
        proxy -> APIGateway : get videolist
        APIGateway --> vlistReq : get
        vlistReq --> vlistService : get
        vlistService -> vlistDb : read
        vlistRep <-- vlistService : videolist
        APIGateway <-- vlistRep : videolist
        proxy <- APIGateway : videolist
        user <- proxy : videolist

        == edit videolist ==

        user -> proxy : add/modify/delete
        proxy -> APIGateway : add/modify/delete
        APIGateway --> vlistReq : add/modify/delete
        vlistReq --> vlistService : add/modify/delete
        vlistService -> vlistDb : add/modify/delete
        vlistRep <-- vlistService : success or fail
        APIGateway <-- vlistRep : success or fail
        proxy <- APIGateway : success or fail
        user <- proxy : success or fail

        == setup suggestion push ==

        user -> proxy : setup suggestions
        proxy -> APIGateway : setup suggestions
        APIGateway --> suggestReq : setup suggestions
        suggestReq --> suggestionService : setup suggestions

        == push suggestions ==

        suggestionService --> vlistReq : get videolist
        vlistReq --> vlistService : videolist
        vlistService -> vlistDb : read
        vlistRep <-- vlistService : videolist
        suggestionService <-- vlistRep : videolist

        suggestionService --> feedReq : get feeddata
        feedReq --> feedService : feeddata
        feedRep <-- feedService : feeddata
        suggestionService <-- feedRep : feeddata

        suggestionService -> suggestionService : generate suggestions \n from \n videolist and feeddata

        suggestRep <-- suggestionService : suggestions
        APIGateway <-- suggestRep : suggestions
        proxy <- APIGateway : suggestions
        user <- proxy : suggestions

        == collect articles ==

        feedService -> externalWebsites : HTTP get
        feedService <- externalWebsites : feeddata

        @enduml

## prospect

- This is choreography without using a saga. 
- The next level could be to come up with some more complex use case which requires a transaction and implement it using orchestration and saga.

# techs

## java EE / Spring

- produce war/ear files for each microservice
- produce images and put them in a container registry
- each microservice has 1 kubernetes pod manifest

## vagrant w/ virtualbox

- create a bunch of VMs (4 presumably)
- create own virtualbox network for the VMs
- each VM is provisioned w/ kubernetes

## kubernetes

- VMs form a kubernetes cluster
- DNS? (kube-dns, coredns, is istio doing that?)

## kafka

- kafka client asks cluster for broker list
- connect to a random one (maybe reconnect to another after a while)

### zookeeper

- there's a pod for zookeeper and one for kafka
- there's a deployment for zookeper and one for kafka
- there's a service for zookeper and one for kafka
- config "zookeeper.connect" is set to the zookeper service
- so, when broker talks to zookeper, k8s DNS selects the zookeeper instance
- https://docs.bitnami.com/tutorials/deploy-scalable-kafka-zookeeper-cluster-kubernetes/

## istio

- connecting, monitoring, and securing
- for each microservice, add a sidecar proxy

## ...